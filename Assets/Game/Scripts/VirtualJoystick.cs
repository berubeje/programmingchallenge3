﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    public NinjaController player;

    private Image outerImage;
    private Image innerImage;
    private Vector2 input;

    private void Start()
    {
        outerImage = GetComponent<Image>();
        innerImage = transform.GetChild(0).GetComponent<Image>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;

        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(outerImage.rectTransform, eventData.position, eventData.pressEventCamera, out pos))
        {
            pos.x = (pos.x / outerImage.rectTransform.sizeDelta.x);
            pos.y = (pos.y / outerImage.rectTransform.sizeDelta.y);

            input = new Vector2(pos.x * 2 + 1, pos.y * 2 - 1);

            if(input.magnitude > 1.0f)
            {
                input = input.normalized;
            }

            innerImage.rectTransform.anchoredPosition = new Vector2(input.x * (outerImage.rectTransform.sizeDelta.x * 0.5f), input.y * (outerImage.rectTransform.sizeDelta.y * 0.5f));
            player.MoveNinja(input);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
 
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        input = Vector2.zero;
        player.MoveNinja(input);
        innerImage.rectTransform.anchoredPosition = Vector3.zero;
    }
}
