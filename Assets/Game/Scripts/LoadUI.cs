﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadUI : MonoBehaviour
{
    public SceneReference sceneToLoad;

    private void Awake()
    {
        SceneLoader.Instance.LoadScene(sceneToLoad, true);
    }
}
