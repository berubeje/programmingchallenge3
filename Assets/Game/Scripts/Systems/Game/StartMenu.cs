﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenu : Menu
{
    public SceneReference sceneToLoad;
    public Camera _camera;

    public void onLoadScene()
    {
        SceneLoader.Instance.LoadScene(sceneToLoad, true);
        MenuManager.Instance.hideMenu(menuClassifier);

        if (_camera != null)
        {
            _camera.enabled = false;
        }
    }
}
