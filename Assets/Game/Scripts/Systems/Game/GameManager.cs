﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    public Text coinValueTxt;
    public Text timeValueTxt;
    public GameObject winScreen;

    private int coinsCollected = 0;
    private float time = 0;

    private void Start()
    {
        if(coinValueTxt != null)
        {
            coinValueTxt.text = coinsCollected.ToString();
        }
    }

    void Update()
    {
        time += Time.deltaTime;

        if(timeValueTxt != null)
        {
            timeValueTxt.text = ((int)time).ToString();
        }
    }

    public void CoinCollected()
    {
        coinsCollected++;

        if (coinValueTxt != null)
        {
            coinValueTxt.text = coinsCollected.ToString();
        }
    }

    public void Win()
    {
        winScreen.SetActive(true);
    }
}
