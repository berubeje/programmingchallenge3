﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaController : MonoBehaviour
{
    public float speed = 5.0f;
    public float movementThreshold = 0.5f;
    public GameObject fireballPrefab;

    private Animator animator;
    private Vector3 inputDirection;
    private Rigidbody2D _rigidbody2D;
    private AudioSource audioSource;

    private bool dontFire = false;
    private Vector2 touchStart;
    private Vector2 touchEnd;

    private bool touchEnded = false;
    private Vector2 swipeMinimum = Vector2.zero;

    void Start()
    {
        animator = GetComponent<Animator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();

        audioSource = GetComponent<AudioSource>();

        swipeMinimum.x = Screen.width * 0.1f;
        swipeMinimum.y = Screen.height * 0.1f;
    }

    void Update()
    {
        GetTouchInput();
    }

    private void GetTouchInput()
    {
        if(dontFire)
        {
            dontFire = false;
            return;
        }

        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                touchStart = touch.position;
            }

            if(touch.phase == TouchPhase.Ended)
            {
                touchEnd = touch.position;
                touchEnded = true;
            }

            if(touchEnded)
            {
                touchEnded = false;
                float distance = Vector2.Distance(touchStart, touchEnd);

                if(distance >= swipeMinimum.x || distance >= swipeMinimum.y)
                {
                    Vector2 direction = (touchEnd - touchStart).normalized;

                    ShootFireball(direction);
                }
            }

        }
    }

    private void ShootFireball(Vector2 direction)
    {
        if (fireballPrefab != null)
        {
            GameObject newFireball = Instantiate(fireballPrefab, transform.position, fireballPrefab.transform.rotation);

            FireballLogic fbLogic = newFireball.GetComponent<FireballLogic>();

            if (fbLogic != null)
            {
                fbLogic.SetDirection(direction.x, direction.y);
            }
        }
    }

    public void MoveNinja(Vector2 direction)
    {

        dontFire = true;

        inputDirection.x = direction.x;
        inputDirection.y = direction.y;

        if (inputDirection.magnitude > movementThreshold)
        {
            _rigidbody2D.velocity = new Vector2(inputDirection.x * speed * Time.deltaTime, inputDirection.y * speed * Time.deltaTime);
            animator.SetFloat("inputX", inputDirection.x);
            animator.SetFloat("inputY", inputDirection.y);
            animator.SetBool("Walking", true);
        }
        else
        {
            animator.SetBool("Walking", false);
            _rigidbody2D.velocity = Vector2.zero;
        }
    }

    public void Die()
    {
        transform.position = Vector2.zero;

        if (audioSource != null)
        {

            audioSource.Play();

        }
    }
}
