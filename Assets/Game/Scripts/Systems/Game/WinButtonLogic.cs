﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinButtonLogic : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<NinjaController>() != null)
        {
            GameManager.Instance.Win();
        }
    }
}
