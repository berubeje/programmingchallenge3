﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class FireballLogic : MonoBehaviour
{
    public float speed;

    private Vector2 direction;
    private Rigidbody2D rig;


    private void Start()
    {
        rig = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        rig.velocity = new Vector2(direction.x * speed * Time.deltaTime, direction.y * speed * Time.deltaTime);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }

    public void SetDirection(float x, float y)
    {
        int dirX;
        int dirY;


        if (x >= 0.5f)
        {
            dirX = 1;
        }
        else if (x <= -0.5f)
        {
            dirX = -1;
        }
        else
        {
            dirX = 0;
        }
          
        if (y >= 0.5f)
        {
            dirY = 1;
        }
        else if (y <= -0.5f)
        {
            dirY = -1;
        }
        else
        {
            dirY = 0;
        }

        if(dirX == 0 && dirY == 0)
        {
            direction = new Vector2(0, -1);
        }
        else
        {
            direction = new Vector2(dirX, dirY);
        }
    }
}
