﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePillarLogic : MonoBehaviour
{
    public float lifeTime = 1.0f;

    private float currentLife = 0.0f;



    private void OnTriggerEnter2D(Collider2D collision)
    {
        NinjaController ninjaController = collision.gameObject.GetComponent<NinjaController>();

        if(ninjaController != null)
        {
            ninjaController.Die();
        }
    }

    void Update()
    {
        currentLife += Time.deltaTime;

        if(currentLife >= lifeTime)
        {
            Destroy(this.gameObject);
        }
    }
}
