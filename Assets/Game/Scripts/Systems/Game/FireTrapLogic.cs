﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTrapLogic : MonoBehaviour
{
    public float executeTime = 2.0f;
    public GameObject firePrefab;

    private float currentExecuteTime = 0.0f;
    private bool triggered;
    private AudioSource audioSource;



    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (triggered)
        {
            currentExecuteTime += Time.deltaTime;

            if(currentExecuteTime >= executeTime)
            {
                Instantiate(firePrefab, transform.position, firePrefab.transform.rotation);
                currentExecuteTime = 0.0f;
                triggered = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
       if(other.gameObject.tag == "Player" && triggered == false)
       {
            triggered = true;

            if (audioSource != null)
            {
                audioSource.Play();
            }
        }
    }
}
